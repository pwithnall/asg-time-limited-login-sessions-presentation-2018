\usepackage{fontspec}
\usepackage{polyglossia}
\setdefaultlanguage{english}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, urlcolor=blue,
            pdftitle=Time-limited login sessions,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}
\usepackage[Latin,Greek,Emoticons]{ucharclasses}
\usefonttheme{professionalfonts} % using non standard fonts for beamer

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

\AtBeginSection{\frame{\sectionpage}}

% Abstract here: https://cfp.all-systems-go.io/ASG2019/talk/8RB73U/
% How Endless are implementing time-limited scopes in systemd, using that to
% implement time-limited login sessions, and then using that to implement
% parental controls on the desktop.
\title{Time-limited login sessions}

\author{Philip Withnall\\Endless\\\texttt{philip@tecnocode.co.uk}}
\date{September 20, 2019}

\begin{document}


\begin{frame}
\titlepage
\end{frame}

\note{5 minutes allocated.}


\begin{frame}{Why do we want time-limited login sessions?}
\begin{itemize}
  \item{Parental controls}
  \item{Digital wellbeing}
\end{itemize}
\end{frame}

\note{I’m going to talk about a small project we’ve been working on over the last
year at Endless and within the wider GNOME community. We are interested in
parental controls for the desktop ­— the ability to prevent (for example)
children from running inappropriate apps, or to limit the amount of time they
spend on the computer or using particular apps, or to limit the times of day
when they are allowed to use the computer. A lot of this applies to ‘digital
wellbeing’ too — applying restrictions to yourself to limit impulsive or excessive
computer use.}

\begin{frame}{How are they implemented?}
\begin{itemize}
  \item{\texttt{RuntimeMaxSec=} property of a scope unit}
  \item{Just like for service units}
  \item{\texttt{systemd.runtime\_max\_sec} PAM setting in \texttt{pam\_systemd}}
\end{itemize}
\end{frame}

\note{One of the key parts of implementing this is having a way to limit the
length of a login session or the amount of time a particular app can be run. That
means adding a feature to systemd to limit the runtime of a systemd scope, since
the login session is a scope.

So we added a \texttt{RuntimeMaxSec=} property to scope units, using the same
semantics as for service units. After the runtime of a particular scope is
exceeded, that scope (and everything in it) will be sent \texttt{SIGTERM} and
then \texttt{SIGKILL}.

We’ve made that settable from PAM modules via the \texttt{pam\_systemd} module:
if \texttt{systemd.runtime\_max\_sec} is set in the PAM data, it will be copied
to \texttt{RuntimeMaxSec=} for the newly created session.

Thanks to Lennart and Yu Watanabe for suggestions and review.}

\begin{frame}{What’s next?}
\begin{itemize}
  \item{Data store}
  \item{A custom PAM module}
  \item{systemd user sessions in production}
\end{itemize}
\end{frame}

\note{What’s next? We need a PAM module to actually set the time limit for the
session, based on the comparison of a system policy for computer use times and
durations, and the user’s recent usage history. That means we need to collect
and store the recent usage history too.

The plan is to collect it for all scopes — which means that, when coupled with
systemd user sessions and flatpak, we will have usage data for apps too, which
can be used for limiting their use.

(Obviously all data will be only stored locally, and deleted after an appropriate
interval.)}

\begin{frame}{What’s next?}
\begin{figure}
\includegraphics[scale=0.5]{wellbeing-and-supervision_clip}
\caption{Digital wellbeing mockup}
\end{figure}
\end{frame}

\note{The UI side of things also needs to be written. Here’s a mockup by Allan
Day of what some of the control centre UI could look like.}

\begin{frame}{What does this allow?}
\begin{itemize}
  \item{Parental controls/Digital wellbeing}
  \item{Terminating over-running process groups (scopes)}
  \item{Kiosk mode}
  \item{CI runners or containers}
\end{itemize}
\end{frame}

\note{Apart from parental controls and digital wellbeing, what does the new
time-limited scope feature allow? It’s quite general-purpose, so various uses of
it could be imagined. For example, limiting the runtime of a group of processes
which you don’t fully trust to ever terminate (CI runners, for example). Or
limiting the length of a session in a kiosk mode computer.}

\begin{frame}{Miscellany}
\begin{description}
  \item[systemd changes]{\url{https://github.com/systemd/systemd/issues/12035}}
  \item[GNOME digital wellbeing designs]{\url{https://gitlab.gnome.org/Teams/Design/settings-mockups/blob/master/wellbeing-and-supervision/}}
\end{description}

\vfill
\begin{center}
{\setsansfont{Noto Emoji}🐦} @pwithnall
\end{center}

% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}
\end{frame}

\end{document}
